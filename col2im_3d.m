function [ out ] = col2im_3d( blocks,psize,imsize )
%COL2IM_3D 3d version of matlabs col2im
% ( blocks,psize,imsize )
% blocks - the vectorized patches to transform
% psize - a vector with the patch size (MUST BE equal)
% ssize - orig matrix size

assert(psize(1) == psize(2),'patches must be square');
assert(psize(3) == psize(2),'patches must be square');

out = zeros(imsize);
Weight= zeros(imsize);
IMout = zeros(imsize);

Weight_2d= zeros(imsize(1),imsize(2),1);
IMout_2d = zeros(imsize(1),imsize(2),1);

xp = psize(1);
yp = psize(2);
zp = psize(3);

pnum_2d2 = (imsize(1)-xp+1)*(imsize(2)-yp+1);

psize_2d = xp*yp;
pnum_2d = size(blocks,1)/psize_2d;

idx = [1:pnum_2d2];
[rows,cols] = ind2sub(size(IMout_2d)-xp+1,idx);

for k = 1:imsize(3)-xp+1
    for j = 1:pnum_2d

        Weight_2d= zeros(imsize(1),imsize(2),1);
        IMout_2d = zeros(imsize(1),imsize(2),1);
        
        frame =  blocks(1+psize_2d*(j-1):psize_2d*(j) , 1+pnum_2d2*(k-1):pnum_2d2*(k));
       
        for i = 1:size(frame,2)
            block =reshape(frame(:,i),[xp,xp]);
            IMout_2d(rows(i):rows(i)+xp-1, cols(i):cols(i)+xp-1) = IMout_2d(rows(i):rows(i)+xp-1, cols(i):cols(i)+xp-1)+block;
            Weight_2d(rows(i):rows(i)+xp-1, cols(i):cols(i)+xp-1)= Weight_2d(rows(i):rows(i)+xp-1, cols(i):cols(i)+xp-1)+ones(xp);
        end
        
        IMout(:,:,j+(k-1)) = IMout(:,:,j+(k-1))+ IMout_2d;
        Weight(:,:,j+(k-1)) = Weight(:,:,j+(k-1)) + Weight_2d;
    end
end

%place it in the correct location
out=IMout./Weight;

end

