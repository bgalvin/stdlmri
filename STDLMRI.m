function [param1,DLMRIparams] = STDLMRI(data,Q1,DLMRIparams)
rng('default')
%Function for reconstructing MR image from undersampled raw (complex) k-space data.  Modified code was based off of the distribution found here
% http://www.ifp.illinois.edu/~yoram/DLMRI-Lab/DLMRI.html


% Outputs -
%       1. Iout1 - Image reconstructed with DLMRI algorithm from undersampled data - only magnitude is output.
%       2. param1 - Structure containing various performance metrics, etc. for DLMRI algorithm.
%                 - Dictionary : Final DLMRI dictionary
%                 - PSNR0 : PSNR of zero-filled reconstruction (output only when ct is set to 1)
%                 - PSNR : PSNR of the reconstruction at each iteration of the DLMRI algorithm (output only when ct is set to 1)
%                 - HFEN : HFEN of the reconstruction at each iteration of the DLMRI algorithm (output only when ct is set to 1)
%                 - itererror : norm of the difference between the reconstructions at successive iterations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DLMRI Parameters initialization
%TVstepsize = DLMRIparams.stepsize;

tvMultiFid = DLMRIparams.weight_fidelity;

num=DLMRIparams.stdlmri_iter;  %DLMRI algorithm iteration count.

%The value of `num' in general depends on how good the initial zero-filling
%reconstruction is. Better initial reconstruction typically implies a smaller value of num.

DLMRIparams.n = prod(DLMRIparams.pshape); % dont touch, this is just total patch size
n=DLMRIparams.n; %patch size

DLMRIparams.K2 = n; 
K2=DLMRIparams.K2; %number of dictionary atoms

N=DLMRIparams.N; %number of training signals
T0=DLMRIparams.T0; %sparsity levels
threshi=DLMRIparams.thr; %error threshold for patches - allows squared error of (threshi^2)*n per patch during sparse coding (see further in code).
numiterKSVD=DLMRIparams.ksvd_iter; %number of K-SVD iterations


%other parameters of K-SVD algorithm
param.errorFlag=0;
param.L=T0;
param.K=K2;
param.numIteration=numiterKSVD;
param.preserveDCAtom=0;
param.InitializationMethod='GivenMatrix';
param.displayProgress=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%MAIN CODE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

I5=data;
[aa,bb]=size(data);
index=find(Q1==1);
I2=(double(I5)).*(Q1);
DLMRIparams.rsize = size(I2);
mymax = 0;

for i=1:size(I2,3)
    I11(:,:,i)=fftshift(ifft2(I2(:,:,i)));
    I5img(:,:,i) = fftshift(ifft2(I5(:,:,i)));
    mymax = [max(max(max(I11(:,:,i))), mymax)];
end

I5=I5./mymax;
I11=I11./mymax; %image, k-space normalization


%DLMRI iterations

for kp=1:num
    tic

    
    Iiter=I11;
    
    blocks = im2col_3d(I11,DLMRIparams.pshape);
    br=mean(blocks); %image patches
    TE=blocks-(ones(n,1)*br);  %subtracting means of patches
    N2=size(blocks,2); %total number of overlapping image patches
    
    de=randperm(N2);
    
    %Check if specified number of training signals is less or greater than the available number of patches.
    if(N2>N)
        N4=N;
    else
        N4=N2;
    end
    YH=TE(:,de(1:N4));  %Training data - using random subset of all patches plus some PCA decomps 
    
    [UH,SH,VH]=svd(YH*YH');
    D0=zeros(n,K2);
    [hh,jj]=size(UH);
    if(K2>=n)
        D0(:,1:jj)=UH;
        p1=randperm(N4);
        for py=jj+1:K2
            D0(:,py)=YH(:,p1(py-jj));
        end
    else
        D0(:,1:K2)=UH(:,1:K2);
    end
	
    param.initialDictionary=D0; %initial dictionary for K-SVD algorithm
    YHU=YH;
    
    [D,~] = KSVDC2(YHU,param,threshi(kp)); 
	thr = threshi(kp);
	Coefs = wrapOMP( D,TE,thr,param.L);
	
	% add back the means by col
    ZZ= D*Coefs + (ones(size(blocks,1),1)) * br;
    
	% transform back from patches as cols
    I3n = col2im_3d(ZZ,DLMRIparams.pshape,DLMRIparams.rsize);
	
	% update 
    DLMRIparams.weight_fidelity = tvMultiFid(kp);
    Itvout = stcr_oneSlice_oneCoil(I5img,I3n, Q1, DLMRIparams);
    
	%save(strcat(num2str(kp),'_internalVars_st','.mat'), '-regexp', '^(?!(|TE|ZZ|blocks|Coefs|ZZ)$).')

	I11 = Itvout;
	
	time= toc;

    disp(strcat( 'Iteration ', num2str(kp) , ' complete ( ',num2str(time),'seconds)'))

end

param1.result=I11; 
param1.Dictionary=D;
param1.iniImg = I5;


