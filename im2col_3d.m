function [patches3d]=im2col_3d(I,psize)
%COL2IM_3D 3d version of matlabs im2col
%   [I,xp,yp,zp] Takes image I and patch size [xp,yp,tp] and creates columns of
%size xp*yp*tp with overlap stride of 1 using I.

assert(psize(1) == psize(2),'patches must be square');
assert(psize(3) == psize(2),'patches must be square');

[xi,yi,zi] = size(I);
xp = psize(1);
yp = psize(2);
zp = psize(3);


num_patch2d=(xi-xp+1)*(yi-yp+1);
num_patch3d=(xi-xp+1)*(yi-yp+1)*(zi-zp+1);
dim_patch2d=(xp*yp);
dim_patch3d=(xp*yp*zp);

for count=1:zi
    tmp_patch2d(:,:,count)=im2col(I(:,:,count),[xp yp],'sliding');
end

count=1;
for slice_counter=1:(zi-zp+1)
    for col_patch=1:num_patch2d
        tmp=[];
        for offset=0:(zp-1)
            tmp=vertcat(tmp,tmp_patch2d(:,col_patch,slice_counter+offset));
        end
        patches3d(:,count)=tmp;
        count=count+1;
    end
end

end



