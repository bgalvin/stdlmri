function [TV_update] = TVG(out_img,beta_sqrd)

% Computing numerator terms
xpy_img = intshft(out_img,[0 -1]); % i+1,j ; p for plus, m for minus
T1_num = xpy_img - out_img;
xmy_img = intshft(out_img,[0 1]); %i-1,j
T2_num = out_img - xmy_img;
xyp_img = intshft(out_img,[-1 0]); %i,j+1
T3_num = xyp_img - out_img;
xym_img = intshft(out_img,[1 0]); %i,j-1
T4_num = out_img - xym_img;

% Generating imgs for computing denominator terms
xmyp_img = intshft(out_img,[-1 1]);
xmym_img = intshft(out_img,[1 1]);
xpym_img = intshft(out_img,[1 -1]);

% Computing denominator terms
T1_den = sqrt(beta_sqrd + abs(T1_num).^2 + (abs((xyp_img - xym_img)/2)).^2);
T2_den = sqrt(beta_sqrd + abs(T2_num).^2 + (abs((xmyp_img - xmym_img)/2)).^2);
T3_den = sqrt(beta_sqrd + abs(T3_num).^2 + (abs((xpy_img - xmy_img)/2)).^2);
T4_den = sqrt(beta_sqrd + abs(T4_num).^2 + (abs((xpym_img - xmym_img)/2)).^2);

% Computing the terms
T1 = T1_num./T1_den;
T2 = T2_num./T2_den;
T3 = T3_num./T3_den;
T4 = T4_num./T4_den;

TV_update = T1-T2+T3-T4;
