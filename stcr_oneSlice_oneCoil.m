
% Program to reconstruct undersampled radial DCE MRI data using temporal and spatial Total Variation constraints

% Ref: G. Adluru, C.J. McGann, P.Speier, E.G. Kholmovski, A. Shaaban, E.V.R. DiBella.
% “Acquisition and reconstruction of undersampled radial data for myocardial perfusion MRI.�?
% Journal of Magnetic Resonance Imaging (JMRI), 29:466-473, 2009.

function recon_data=stcr_oneSlice_oneCoil(orig_kspace,reduced_k_space,mask_k_space_sparse, params)
params.debugFlag=1;
beta_sqrd = 1e-16;
nt=size(reduced_k_space,3);
% period after which offset angle repeats

%disp('Pre-interpolating data to Cartesian grid ')

%[reduced_k_space mask_k_space_sparse]=pre_interp(k_space,params.offset_period);
%[full_k_space mask_full_k_space_sparse]=pre_interp(k_space,2);

measuredImgDomain=zeros(size(reduced_k_space));
%fullImgDomain=zeros(size(reduced_k_space));
currentEst=zeros(size(reduced_k_space));

% for i=1:nt
%     measuredImgDomain(:,:,i) = fftshift(ifft2(orig_kspace(:,:,i)));
%     currentEst(:,:,i) = (ifft2(reduced_k_space(:,:,i))); 
%     mask_k_space_sparse(:,:,i) = fftshift(mask_k_space_sparse(:,:,i));
% end

measuredImgDomain = orig_kspace;
currentEst = reduced_k_space;


temporal_term_update=zeros(size(measuredImgDomain));
img_est=currentEst;
W_img_est=ifft2(fft2(img_est).*mask_k_space_sparse);

if(params.debugFlag==1)
    fidelityNorm=zeros(params.stcr_iter,1);
    temporalNorm=zeros(params.stcr_iter,1);
    spatialNorm=zeros(params.stcr_iter,1);
    total_cost=zeros(params.stcr_iter,1);
end

for iter_no=1:params.stcr_iter

    fidelity_update=params.weight_fidelity*(measuredImgDomain-W_img_est);
    
    % Computing temp regul - TV
    temp_a=diff(img_est,1,3);
    temp_b=temp_a./(sqrt(beta_sqrd+(abs(temp_a).^2)));
    temp_c=diff(temp_b,1,3);
    
    temporal_term_update(:,:,1)=temp_b(:,:,1);
    temporal_term_update(:,:,2:end-1)=temp_c;
    temporal_term_update(:,:,end)=-temp_b(:,:,end);
    
    temporal_term_update=temporal_term_update*params.weight_temporal;
    
    % Computing spatial regul - TV
    spatial_term_update=TV_Spatial_2D(img_est,beta_sqrd)*params.weight_spatial;
    
    if(params.debugFlag==1)
        fidelityNorm(iter_no)=sum(sum(sum(abs(fidelity_update).^2)));
        temporalNorm(iter_no)=sum(sum(sum(abs(diff(img_est,1,3)))));
        
        % spatial norm:  (note matlab gradient() call not the same as diff
        trunc=size(img_est,1)-1;
        sx_norm=abs(diff(img_est,1,2));
        sx_norm=sx_norm(1:trunc,1:trunc,:);
        sy_norm=abs(diff(img_est,1,1));
        sy_norm=sy_norm(1:trunc,1:trunc,:);
        spatialNorm(iter_no)=sum(sum(sum(sqrt(abs(sx_norm).^2+abs(sy_norm).^2))));
        
        figure(99);clf; hold on;
        plot(params.weight_fidelity*fidelityNorm(1:iter_no),'c*-')
        plot(params.weight_temporal*temporalNorm(1:iter_no),'kx-');
        plot(params.weight_spatial*spatialNorm(1:iter_no),'bx-');
        totalNorm=params.weight_temporal*temporalNorm(1:iter_no)+params.weight_fidelity*fidelityNorm(1:iter_no)+params.weight_spatial*spatialNorm(1:iter_no);
        plot(totalNorm)
        legend('fidelity (squared L2)','temporal (L1 norm)','spatial (L1 norm)',  'total')
        title('All curves are norm times weight ')
        drawnow
    end
    
    
    img_est   = img_est+params.stepsize*(fidelity_update+temporal_term_update+spatial_term_update);
    W_img_est = ifft2(fft2(img_est).*mask_k_space_sparse);
    
%     curKS = fft2(img_est)
%     curKS(idx) = 
    
    if(params.debugFlag==2)   % write out recon every 30 iterations.
        if mod(iter_no,30)==0
            sx=size(img_est,1);
            recon_data=zeros(sx/2,sx/2,size(img_est,3));
            for i=1:size(recon_data,3)
                recon_data(:,:,i)=rot90(abs(img_est(sx/4+1:3*sx/4,sx/4+1:3*sx/4,i)),-1);
            end
            outfile=strcat('test_iter',int2str(iter_no),'_F',int2str(1000*params.weight_fidelity),'_T',int2str(1000*params.weight_temporal),'_S',int2str(1000*params.weight_spatial))
            stepsize = params.stepsize;
            save(outfile,'recon_data', 'fidelityNorm', 'temporalNorm', 'spatialNorm', 'totalNorm','stepsize');
        end
    end
end

%crop to remove 2x read direction oversampling
sx=size(img_est,1);

%recon_data=zeros(sx/2,sx/2,size(img_est,3));
recon_data=zeros(size(img_est));

for i=1:size(img_est,3)
    %recon_data(:,:,i)=rot90(abs(img_est(sx/4+1:3*sx/4,sx/4+1:3*sx/4,i)),-1);
    recon_data(:,:,i) = ((img_est(:,:,i)));
end

if(params.debugFlag==19)
%     figure(100);clf; hold on;
%     subplot(2,2,1); plot(params.weight_fidelity*fidelityNorm,'c*-'); title('Fidelity norm')
%     subplot(2,2,2); plot(params.weight_temporal*temporalNorm,'kx-');  title('Temporal norm')
%     subplot(2,2,3); plot(spatialNorm,'bx-'); title('Spatial norm')
    figure(1000)
    for iii = 1:nt
        pause
        
        subplot(2,3,1);
        imagesc( abs( fullImgDomain(:,:,iii)))
        subplot(2,3,2);
        imagesc( abs( measuredImgDomain(:,:,iii)))
        subplot(2,3,3);
        imagesc( abs ( img_est(:,:,iii)))
        subplot(2,3,5);
        imagesc( abs ( mask_full_k_space_sparse(:,:,iii)))
        subplot(2,3,6);
        imagesc( abs ( mask_k_space_sparse(:,:,iii)))
    end
    
end


return;

