clear all
close all


% Use the pincat phantom for example
load aperiodic_pincat2
image = f;
normed_img = image./max(max(max(image)));

for i = 1:50
	mask_k_space_sparse(:,:,i) = ifftshift(genRandMask(size(f(:,:,1)),10,1,.7));
end

%mask_k_space_sparse=repmat(mask,[1,1,size(image,3)]);

noise = .1*(complex(randn(size(mask_k_space_sparse)),randn(size(mask_k_space_sparse))));

noisy_kspace = fft2(ifftshift(normed_img))+noise;
masked_noisy_kspace= noisy_kspace.*mask_k_space_sparse;

noisy_img  = ifft2(noisy_kspace);
masked_noisy_image = ifft2(masked_noisy_kspace);

undersampledKspace = masked_noisy_kspace;
undersamplingMask = mask_k_space_sparse;

%%%%%%  ALGO setup 
% Path to SPAMS install
spamsPath = '/home/sci/bgalvin/projects/dict_learning/spams-matlab';

% add spams path and ini things we need
addpath(genpath(spamsPath))

% We will try to use a worker pool.
try
    matlabpool close force local;
catch
end
try
    myCluster = parcluster('local');
    matlabpool(myCluster.NumWorkers);
catch
    exit;
end

% PATCH GENERATION
params.pshape = [5 5 5]; %patch size (x,y,t) must be a cube
params.sshape = [1 1 1]; %overlap stride, most use 1 1 1

% ITERATIONS
params.stdlmri_iter = 20; % Outer loop iterations
params.ksvd_iter = 10; % KSVD training iterations

% KSVD PARAMS
params.N = 750; % Dictionary size
params.T0 = 10;  % OMP and KSVD sparsity constraint
params.thr = linspace(.07,.03,params.stdlmri_iter );   % Monotonically decreasing L2 thresh for KSVD

% TV PARAMS
params.stcr_iter       = 30; % TV recon iterations
params.stepsize        = .3; % TV recon step size
params.weight_fidelity = linspace(.7,.3,params.stdlmri_iter); % Monotonically decreasing L2 thresh for TV
params.weight_temporal =.001;   % regularization parameter 1
params.weight_spatial  =.001;  % regularization parameter 2

[out,extra_info] = STDLMRI(undersampledKspace,undersamplingMask,params);

save(strcat('stdlmri_output_',num2str(now),'.mat'))
