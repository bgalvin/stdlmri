function [ mask ] = genRandMask( size, underX,weightedG,wid )
%GENRANDMASK( size, underX, weightedG, wid ) Generates a random or 
%weighted (weightedG) mask of size (size) and undersampling factor (underX). 
%If Gaussian weighted set the dist width with (wid) 
%(ONLY FOR 2D right
%now)
percentageUnder = 1/underX;

mask = zeros(size);

while sum(sum(mask))/(size(1)*size(2)) < percentageUnder
    
    if weightedG
        rng1 = round(size(1)/2+(sqrt(size(1)*(wid))*randn(1)));
        rng2 = round(size(2)/2+(sqrt(size(2)*(wid))*randn(1)));
    else
        rng1 = round(size(1)*rand(1));
        rng2 = round(size(2)*rand(1));
    end
    
    
    if rng1 >=1 && rng1 <= size(1) && rng2 >=1 && rng2 <= size(2) && mask(rng1,rng2) == 0
        
        mask(rng1,rng2) = true;
        
    end
end


end


