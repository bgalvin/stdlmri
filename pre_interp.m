% function to pre-interpolate radial data onto a cartesian grid

function [cart_k_space_sparse_all mask_k_space_sparse_all]=pre_interp(sparse_radial_data,sparse_factor)

[sx sy sz]=size(sparse_radial_data);

count_var=0;

X=zeros(sx,sy);
Y=zeros(sx,sy);

X_all=zeros(sx,sy,sz);
Y_all=zeros(sx,sy,sz);

temp_X_all=zeros(sx,sy,sz);
temp_Y_all=zeros(sx,sy,sz);

cart_k_space_sparse_all=zeros(sx,sx,sz);
mask_k_space_sparse_all=zeros(sx,sx,sz);

for noi=1:sz
    
    clear X
    clear Y
    
    l = 1;
    pha = count_var*pi/(sy*sparse_factor) + pi;
    
    for counter=1:sy
        
        theta_radian=(counter-1)*pi/sy+pha;
        
        k=1;
        for r=-(sx/2-1):sx/2
            [x y]=pol2cart(theta_radian,r);
            X(k,l)=x;
            Y(k,l)=y;
            k=k+1;
        end
        l=l+1;
    end
    
    if(sparse_factor>1)
        count_var=count_var+1;
        if(count_var==sparse_factor)
            count_var=0;
        end
    end
    
    X_all(:,:,noi)=X;
    Y_all(:,:,noi)=Y;
    
end

count_var=0;

for noi=1:sz
    
    img=sparse_radial_data(:,:,noi);
    
    clear Z
    Z=(flipud(img));
    
    clear X
    clear Y
    X=X_all(:,:,noi);
    Y=Y_all(:,:,noi);
    
    clear XI
    clear YI
    XI=round(X);
    YI=round(Y);
    warning off
    
    clear ZI
    
    ZI=griddata(X,Y,Z,XI,YI);
    
    KI=zeros(size(ZI));
    ii=find(ZI>0);
    jj=find(ZI<=0);
    
    KI(ii)=ZI(ii);
    KI(jj)=ZI(jj);
    
    clear cart_k_space
    cart_k_space=KI;
    
    cart_k_space_sparse=zeros(sx,sx);
    mask_k_space_sparse=zeros(sx,sx);
    
    clear temp_X
    clear temp_Y
    
    temp_X=XI+sx/2;
    temp_Y=YI+sx/2;
    
    temp_X_all(:,:,noi)=temp_X;
    temp_Y_all(:,:,noi)=temp_Y;
    
    for i=1:sx
        for j=1:sy
            try
                cart_k_space_sparse(temp_X(i,j),temp_Y(i,j))=cart_k_space(i,j);
                mask_k_space_sparse(temp_X(i,j),temp_Y(i,j))=1;
            catch
            end
        end
    end
    
    cart_k_space_sparse_all(:,:,noi)=fftshift(cart_k_space_sparse);
    mask_k_space_sparse_all(:,:,noi)=fftshift(mask_k_space_sparse);
    
    if(sparse_factor>1)
        count_var=count_var+1;
        if(count_var==sparse_factor)
            count_var=0;
        end
    end
    
end

return;
