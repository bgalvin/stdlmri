function [ Coefs ] = wrapOMP( D,x,thr,L)
%WRAPOMPERRN A multi worker wrapper for OMP.  The dictionary columns must be normed to 1
%   ( D,X,errorGoal,T0m )
% D - Dictionary 
% X - signal (patches in cols) to encode
% thr - L2 encoding error cutoff
% L - Sparsity cutoff (number of atoms to use for encoding)
numAtoms = size(D,2);

% using a trick to encode complex signals using a real value OMP function
DDD = [real(D) -imag(D); imag(D) real(D)];
xxx = [real(x); imag(x)];

param.L = L;
param.thr = thr;

Coefs = sparse(zeros(size(D,2),size(x,2)));

	parfor idx = 1:size(xxx,2)
	
		cur_b = xxx(:,idx);
	
		Coefs_big = mexOMP(cur_b,DDD,param);
		Coefs(:,idx) = sparse( complex( full( Coefs_big(1:numAtoms,:) ), full( Coefs_big( (numAtoms+1):end,:) )));
		
	end
end

