This is an implementation of STDLMRI.
To run this program you must have MATLAB with access to the image processing toolbox.  If
 you do not have access to this toolbox you can rewrite the im2col and col2im functions which should remove this requirement.  
STDLMRI can make use of MATLABs worker pool for the OMP encoding steps.  If this is not available then it will simply run OMP serially.

Files included:
	STDLMRI_harness.m  This is the main function harness.  Look here for an example of how to run the program.
	STDLMRI.m  The main function (Adapted from http://www.ifp.illinois.edu/~yoram/DLMRI-Lab/DLMRI.html)
	col2im_3d.m  transforms a 3D volume into a vertical column matrix made up of overlapping cube patches from the volume.
	im2col_3d.m  undoes col2im_3d.m
	KSVDC2.m  Implements KVSD dictionary learning for complex data.  (Adapted from http://www.ifp.illinois.edu/~yoram/DLMRI-Lab/DLMRI.html)
	stcr_oneSlice_oneCoil.m  Impliments STCR as outlined as outlined here https://www.sci.utah.edu/software/stcr.html for L2+TV update step.
	TVG.m  helper for Stcr_oneSlice_oneCoil.m, complutes TV gradient
	TV_Spatial_2D  helper for Stcr_oneSlice_oneCoil.m, computes spatial TV for one slice.
	wrapOMP.m  A wrapper for the OMP process that makes use of MATLAB worker pool.  Changing the reconstruction method here (OMP, Lasso, etc
) will change it for the entire algorithm.


To run the example open a matlab command line and type "run STDLMRI_harness"